# Generated by Django 3.2.6 on 2021-08-16 07:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_alter_submission_activitie'),
    ]

    operations = [
        migrations.AlterField(
            model_name='submission',
            name='grade',
            field=models.IntegerField(null=True),
        ),
    ]
