from django.db import models
from django.db.models.fields.related import ForeignKey

from django.contrib.auth.models import User

class Course(models.Model):
    name = models.CharField(max_length=255, unique=True)
    users = models.ManyToManyField(User, related_name='course')
    
    def __str__(self):
        return f'{self.name}'

class Activity(models.Model):
    title = models.CharField(max_length=255, unique=True)
    points = models.IntegerField()

class Submission(models.Model):
    grade = models.IntegerField(null=True)
    repo = models.CharField(max_length=255,)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="submission")
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE, related_name="submissions")