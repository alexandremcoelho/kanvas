from rest_framework.permissions import BasePermission, SAFE_METHODS

class OnlyAdminAndStaff(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_superuser or request.user.is_staff        
            
class OnlyCAdminCreate(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'GET':
            return True
        
        if request.method == 'POST' or request.method == 'PUT':
            return request.user.is_superuser or request.user.is_staff

        if request.method == 'DELETE':
            return request.user.is_superuser 

class CoursePermission(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'GET':
            return True
        
        if request.method == 'POST':
            return request.user.is_superuser 
    
    
class StaffOnlyGet(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'GET':
            return request.user.is_staff
        
        if request.method == 'POST':
            return request.user.is_superuser
    
class NeverPermit(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_superuser or request.user.is_staff:
            return False
        return True

class SubmissionPermission(BasePermission):
    def has_permission(self, request, view):
        if not request.user.is_anonymous:
            return True

