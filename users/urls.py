from django.urls import path, include
from .views import UserView, LoginView, CourseView, RegistrationView, ActivityView, submissionView, EditsubmissionView, AllsubmissionView, FilterCourseView


urlpatterns = [
    path("accounts/", UserView.as_view()),
    path("login/", LoginView.as_view()),
    path("courses/", CourseView.as_view()),
    path("courses/<int:course_id>/", FilterCourseView.as_view()),
    path("courses/<int:course_id>/registrations/", RegistrationView.as_view()),
    path("activities/", ActivityView.as_view()),
    path("activities/<int:activity_id>/submissions/", submissionView.as_view()),
    path("submissions/", AllsubmissionView.as_view()),
    path("submissions/<int:submission_id>/", EditsubmissionView.as_view()),

]
















