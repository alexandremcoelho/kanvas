from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework import status
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from users.models import Course, Activity, Submission
from users.serializers import UserSerializer, LoginSerializer, CourseSerializer, OutputCourseSerializer, OutputUserSerializer, ActivitySerializer, SubmissionSerializer
from rest_framework.authentication import TokenAuthentication
from users.permissions import OnlyCAdminCreate, NeverPermit, CoursePermission, OnlyAdminAndStaff, SubmissionPermission
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, IsAdminUser 


class UserView(APIView):
    def get(self, request):
        user = User.objects.all()
        serialized = UserSerializer(user, many=True)
        return Response(serialized.data)

    def post(self, request):
        serializer = UserSerializer(data=request.data)
        
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        if User.objects.filter(username=serializer.validated_data['username']).exists():
            return Response({'error': 'user already exists'}, status=status.HTTP_409_CONFLICT)

        validated_data = serializer.validated_data
        user = User.objects.create_user(**validated_data)
        return Response(OutputUserSerializer(user).data, status=status.HTTP_201_CREATED)

class LoginView(APIView):
    def post(self, request):
        serializer = LoginSerializer(data=request.data)
        
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        user = authenticate(**serializer.validated_data)
        
        if user:
            token = Token.objects.get_or_create(user=user)[0]
            return Response({'token': token.key})
        else:
            return Response({'Unauthorized': 'Failed to authenticate'}, status=status.HTTP_401_UNAUTHORIZED)

class CourseView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [CoursePermission]

    def get(self, request):
        courses = Course.objects.all()
        serializer = CourseSerializer(courses, many=True)
        return Response(serializer.data)

    def post(self, request):
        course = Course.objects.get_or_create(name=request.data['name'])[0]
        serialized = CourseSerializer(course)
        return Response(serialized.data, status=status.HTTP_201_CREATED)

class FilterCourseView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [OnlyCAdminCreate]
    def get(self, request, course_id):
        try:
            if course_id:
                course = Course.objects.get(id=course_id)
                serializer = CourseSerializer(course)
                return Response(serializer.data)
        except:
            return Response({"errors": "invalid course_id"}, status=status.HTTP_404_NOT_FOUND)
    def delete(self, request, course_id):
        try:
            if course_id:
                course = Course.objects.get(id=course_id).delete()
                return Response(status=status.HTTP_204_NO_CONTENT)
        except:
            return Response({"errors": "invalid course_id"}, status=status.HTTP_404_NOT_FOUND)

class RegistrationView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [OnlyCAdminCreate]

    def put(self, request, course_id):

        user_ids = request.data['user_ids']
        if type(user_ids) != list:
            return Response( status=status.HTTP_400_BAD_REQUEST)
        
        try:
            course = Course.objects.get(id=course_id)         
        except:
            return Response({"errors": "invalid course_id"}, status=status.HTTP_404_NOT_FOUND)


        user_list = []
        for id in user_ids:
            try:
                user = User.objects.get(id=id)
            except:
                return Response({"errors": "invalid user_id list"}, status=status.HTTP_404_NOT_FOUND)

            if user.is_staff or user.is_superuser:
                return Response({"errors": "Only students can be enrolled in the course."},status=status.HTTP_400_BAD_REQUEST)

            user_list.append(user)

        course.users.set(user_list)
        return Response(OutputCourseSerializer(course).data)

class ActivityView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [OnlyAdminAndStaff]
    def get(self, request):
        activities = Activity.objects.all()
        serialized = ActivitySerializer(activities, many=True)
        return Response(serialized.data)
    def post(self, request):
        activity = Activity.objects.get_or_create(**request.data)[0]
        serialized = ActivitySerializer(activity) 
        return Response(serialized.data, status=status.HTTP_201_CREATED)

class submissionView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [NeverPermit]
    def post(self, request, activity_id):
        try:
            activity = Activity.objects.get(id=activity_id)         
        except:
            return Response({"errors": "invalid activity_id"}, status=status.HTTP_404_NOT_FOUND)

        user = User.objects.get(username=request.user)         
        obj = {
            "repo": request.data['repo'],
            "activity": activity,
            "user": user
        }
        submission = Submission.objects.create(**obj)
        serialized = SubmissionSerializer(submission)
        return Response(serialized.data, status=status.HTTP_201_CREATED)

class AllsubmissionView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [SubmissionPermission]
    def get(self, request):
        if request.user.is_staff or request.user.is_superuser:
            submission = Submission.objects.all()
            serialized = SubmissionSerializer(submission, many=True)
            return Response(serialized.data)
            
        submission = Submission.objects.filter(user=request.user)
        print(submission)
        serialized = SubmissionSerializer(submission, many=True)
        return Response(serialized.data)

class EditsubmissionView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [OnlyAdminAndStaff]
    def put(self, request, submission_id):
        try:
            submission = Submission.objects.get(id=submission_id)     
        except:
            return Response({"errors": "invalid submission_id"}, status=status.HTTP_404_NOT_FOUND)

        submission.grade = request.data['grade']
        submission.save()
        serialized = SubmissionSerializer(submission)
        return Response(serialized.data) 